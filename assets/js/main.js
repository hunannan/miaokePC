/*
 * @Author: zengzeping
 * @Date: 2019-09-05 10:47:19
 * @LastEditors: zengzeping
 * @LastEditTime: 2019-10-29 10:33:19
 * @Description: file content
 */

let $body = $("body");
$(function() {


    //导航path
    let path = ["index", "product",
        "project", "about"
    ];

    $("header").load("./assets/tmp/head.html", function(res) {
        let _pathname = location.pathname

        let index = path.findIndex(el => {
            return _pathname.includes(el);
        });

        if (_pathname == "/" || _pathname == "/miaokepc/") {
            index = 0
        }
        if (index == 3) {
            $(this).addClass("nor-theme")
            $("header .logo img").attr("src", "./assets/images/logo-nor.svg")
        }
        // 导航
        $(".menu .menu-item")
            .eq(index)
            .addClass("active")
            .siblings()
            .removeClass("active");

        if (_pathname.lastIndexOf("_") > 0) {
            let childIndex = _pathname.substring(_pathname.lastIndexOf("_") + 1, _pathname.lastIndexOf("_") + 2)
            $('.submenu .link').eq(childIndex - 1).addClass("active")
                .siblings()
                .removeClass("active");

        }

        //移动端菜单弹出
        let $menu = $(".menu");
        $(".button-menu").on("click", function() {
            if ($menu.hasClass("active")) {
                closeMask();
                $menu.removeClass("active");
                $menu.find("ul").removeClass("show")
            } else {
                showMask($("header .container"));
                $menu.addClass("active");
                $menu.find("ul").addClass("show")
                $(".has-submenu")
                    .animate({
                        height: "200px"
                    })
                    .addClass("click");

                let $mask = $('.mask')
                $mask.on("click", function() {
                    closeMask()
                    $menu.removeClass("active");
                    $menu.find("ul").removeClass("show")
                })
            }
            // $(".menu ul").toggleClass("show");
        });

        // 移动端
        if (screen.width < 767) {
            $(".has-submenu").on("click", function() {
                if ($(this).hasClass("click")) {
                    $(this).animate({
                            height: "50px"
                        })
                        .removeClass("click");
                } else {
                    $(this)
                        .animate({
                            height: "200px"
                        })
                        .addClass("click");
                }
            });
        }

    });


    // imgLazyLoad($("img[data-src]"));


    $(".footer").load("./assets/tmp/foot.html");

    location.pathname.includes("project") ? getSeverData() : "";

    // 首页轮播
    if ($(".swiper-banner").length > 0) {
        var banner = new Swiper(".swiper-banner .swiper-container", {
            watchOverflow: true,
            autoplay: {
                delay: 3000,

            },
            pagination: {
                el: '.banner-pagination',
                clickable: true,
            },
        });
    };
    // 带指示器的轮播
    if ($(".cus-swiper").length > 0) {
        var swiperSlideTo = new Swiper(".cus-swiper .swiper-container", {
            direction: "vertical",
            allowTouchMove: false
        });
        $(".nav-swiper li").on("mouseenter", function() {
            $(this)
                .addClass("active")
                .siblings()
                .removeClass("active");
            swiperSlideTo.slideTo($(this).index());
        });
    };

    if (document.hasOwnProperty("ontouchstart") || screen.width < 767) {
        $body.attr("ontouchstart", "");
    } else {
        $body.removeAttr("ontouchstart");
    };

});



function getSeverData() {
    let pathId = location.pathname;
    let colum = 24;
    // 特色服务瀑布流
    $.ajax({
        type: "get",
        url: "./assets/data/data.json",
        data: "data",
        dataType: "json",
        success: function(res) {
            let severData = res.data.sever.find(el => {
                return pathId.includes(el.id);
            });
            // console.log(severData);
            inntSeverCard(severData);
            screen.width <= 424 ? colum = 8 : colum
                // console.log(screen.width);

            minigrid(".sjs-default", ".card-box", colum);
            window.addEventListener('resize', function() {
                screen.width < 424 ? colum = 8 : colum
                minigrid(".sjs-default", ".card-box", colum);
            });
        }
    });
};

function inntSeverCard(data) {
    let card = data.child.map(el =>
            `<div class="card-box">
                    <div class="img-box">
                        <div class="text text-center">
                            <h3>${el.title}</h3>
                            <span class="opacity-4">${el.des}</span>
                        </div>
                        <img class="" alt="${el.title}" srcset="./assets/images/sever/${el.url}@2x.png 2x" src="./assets/images/sever/${el.url}.png" > 
                    </div>
                    <ul class="flex-between ul-style flex-wrap">
                     ${el.item.map(item =>
                         `<li>
                            <div class="text">
                                <h5>${item.title}</h5>
                                <span class="opacity-4">${item.des}</span>
                            </div>
                        </li>`).join("")}
                     </ul>
                  </div>`
    ).join("");
    // console.log(card);
    $(".sjs-default").html(card);


};


//弹窗的方法
function openDialog() {
    let dialog = `<div class="dialog"><div class="close"></div><div class="qrcode"><img src="./assets/images/QRcode-kf.png" alt=""></div></div>`;
    showMask();
    $body.append(dialog);

    $(".mask,.dialog .close").on("click", function () {
        closeDialog();
    });
};

function closeDialog() {
    $(".dialog").remove();
    closeMask();
};

//黑色遮罩层的方法
function showMask(dom) {
    if (!dom) {
        dom = $body;
    }
    let mask = `<div class="mask"></div>`;
    dom.append(mask);

    // $body.css('top', -$(window).scrollTop())
    $body.addClass("overflow");
};

function closeMask() {
    $(".mask").remove();
    $body.removeClass("overflow");
};